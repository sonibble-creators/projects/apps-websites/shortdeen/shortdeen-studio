export interface SanitySchemaOptions extends Record<string, any> {
  list?: any[]
  layout?: string
  direction?: string
}

export interface SanitySchemaField extends Record<string, any> {
  name: string
  title: string
  type: string
  description?: string
  initialValue?: any
  options?: SanitySchemaOptions
  validation?: any
  of?: any[]
}

export interface SanitySchemaPreviewSelect extends Record<string, any> {
  title?: string
  subtitle?: string
  media?: string
}

export interface SanitySchemaPreview extends Record<string, any> {
  select?: SanitySchemaPreviewSelect
  prepare?: (any) => SanitySchemaPreviewSelect
}

export interface SanitySchema extends Record<string, any> {
  name: string
  title: string
  type: string
  fields: SanitySchemaField[]
  preview?: SanitySchemaPreview
}
