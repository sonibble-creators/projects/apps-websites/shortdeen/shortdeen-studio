import { SanitySchema } from '../type'

/**
 * # userSchema
 *
 * the schema for user
 * that contain info about the current user
 */
const userSchema: SanitySchema = {
  name: 'user',
  title: 'Users',
  type: 'document',
  fields: [
    {
      name: 'identifier',
      title: 'Identifier',
      type: 'string',
    },
    {
      name: 'password',
      title: 'Password',
      type: 'string',
    },
    {
      name: 'status',
      title: 'Status',
      type: 'string',
      options: {
        list: [
          { title: 'Active', value: 'active' },
          { title: 'Deactive', value: 'deactive' },
        ],
        layout: 'radio',
      },
    },
    {
      name: 'role',
      title: 'Role',
      type: 'string',
      options: {
        list: [
          { title: 'User', value: 'user' },
          { title: 'Creator', value: 'creator' },
          { title: 'Admin', value: 'admin' },
        ],
        layout: 'radio',
      },
    },
    {
      name: 'provider',
      title: 'Provider',
      type: 'array',
      of: [{ type: 'string' }],
      options: {
        list: [
          { title: 'Google', value: 'google' },
          { title: 'Password', value: 'password' },
        ],
      },
    },
  ],
  preview: {
    select: {
      identifier: 'identifier',
      provider: 'provider',
      status: 'status',
      role: 'role',
    },
    prepare: (selection) => {
      const { identifier, provider, status, role } = selection
      return {
        title: identifier,
        subtitle: `${provider} - ${status} - ${role}`,
      }
    },
  },
}

export default userSchema
