import { SanitySchema } from '../type'

/**
 * # profileSchema
 *
 * the schema data for profile
 * to save and manage the profile detail
 *
 */
const profileSchema: SanitySchema = {
  name: 'profile',
  title: 'Profiles',
  type: 'document',
  fields: [
    {
      name: 'fullName',
      title: 'Full Name',
      type: 'string',
    },
    {
      name: 'email',
      title: 'Email address',
      type: 'string',
    },
    {
      name: 'username',
      title: 'Username',
      type: 'string',
    },
    {
      name: 'location',
      title: 'Location',
      type: 'string',
    },
    {
      name: 'birthDate',
      title: 'Birth Date',
      type: 'datetime',
    },
    {
      name: 'bio',
      title: 'Bio',
      type: 'text',
    },
    {
      name: 'cover',
      title: 'Cover',
      type: 'image',
    },
    {
      name: 'avatar',
      title: 'Avatar',
      type: 'image',
    },
    {
      name: 'user',
      title: 'user',
      type: 'reference',
      to: { type: 'user' },
    },
  ],
  preview: {
    select: {
      media: 'avatar',
      title: 'fullName',
      subtitle: 'username',
    },
  },
}

export default profileSchema
