// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator'
import schemaTypes from 'all:part:@sanity/base/schema-type'
import userSchema from './user/user'
import profileSchema from './profile/profile'
import linkSchema from './links/link'

/**
 * # createSchema
 *
 * create the studio schema design for model
 * data and generated it become the Frontend Design In Studio
 *
 */
export default createSchema({
  name: 'shortdeen-default',
  types: schemaTypes.concat([
    // add your content here
    userSchema,
    profileSchema,
    linkSchema,
  ]),
})
