import { SanitySchema } from '../type'

/**
 * # linkSchema
 *
 * the schema to manage and handle
 * all of the shorten link and including the meta data
 */
const linkSchema: SanitySchema = {
  name: 'link',
  title: 'Links',
  type: 'document',
  fields: [
    {
      name: 'shortLink',
      title: 'Short Link',
      type: 'string',
    },
    {
      name: 'sourceLink',
      title: 'Source Link',
      type: 'string',
    },
    {
      name: 'totalView',
      title: 'Total Views',
      type: 'number',
      initialValue: 0,
    },
    {
      name: 'status',
      title: 'Status',
      type: 'string',
      options: {
        list: [
          { title: 'Active', value: 'active' },
          { title: 'Disable', value: 'disable' },
        ],
        layout: 'radio',
      },
    },
    {
      name: 'source',
      title: 'Sources',
      type: 'string',
    },
    {
      name: 'owner',
      title: 'Owner',
      type: 'reference',
      to: [{ type: 'user' }],
    },
  ],
  preview: {
    select: {
      title: 'shortLink',
      subtitle: 'status',
    },
  },
}

export default linkSchema
